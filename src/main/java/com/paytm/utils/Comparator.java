package com.paytm.utils;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.GsonJsonProvider;

import io.restassured.response.Response;

public class Comparator {
	private File logFile ;
	private  File comparisonResultFile;
	private FileWriter logWriter;
	private FileWriter crWriter;
	private String dirName;

	private long logFileMaxSizeAllowedInKB = Integer.parseInt(ConfigReader.getValue("logFileMaxSize"));
	private  long comparisonResultMaxSizeAllowedInKB = Integer.parseInt(ConfigReader.getValue("comparisonResultMaxSize"));
	private static final String server1 = ConfigReader.getValue("server1");
	private static final String server2 = ConfigReader.getValue("server2");
	private String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyz0123456789";
	private Configuration conf = Configuration.builder().jsonProvider(new GsonJsonProvider()).build();
	private HashSet<String> urlsTraversed = new HashSet<>();
	
	int passCount=0;
	int failCount=0;
	int exceptionCount=0;
	int totalCount=0;


	public Comparator(String dirName) throws IOException {
		this.dirName = dirName;
		logFile = getNewLogFile();
		logWriter = new FileWriter(logFile);
		comparisonResultFile = getNewComparisonFile();
		crWriter = new FileWriter(comparisonResultFile);
	}


	public void compare(String url) throws IOException {

		if(!urlsTraversed.contains(url)) {
			System.out.println("Replaying request for url -- "+url);
			boolean isExceptionOccured=false;
			totalCount++;
			try {
				String requestID = requestIDGenerator();
				if(!url.startsWith("/"))
					url+="/";
				Response response_server1  =  given()
						.when()
						.get("http://"+server1+url)
						.then()
						.extract().response();

				Response response_server2 = given()
						.when()
						.get("http://"+server2+url)
						.then()
						.extract().response();

				boolean differs = false;
				writeToLogFile(requestID);
				writeToComparisonFile("\n\nComparison result for request   "+requestID);
				writeToComparisonFile("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				if(response_server1.getStatusCode() != response_server2.getStatusCode()) {
					writeToLogFile("     had differing status code");
					writeToComparisonFile("Differing Status server1 and server2 "
							+response_server1.getStatusCode()+"  ----  "+response_server2.getStatusCode());
					differs = true;
					writeToFiles(requestID, url, response_server1.asString(), response_server2.asString());
					failCount++;

				}else {
					String leftJson = "";
					String rightJson = "";
					try{
							String leftJsonFull =  response_server1.asString();
							String rightJsonFull = response_server2.asString();


							if(JsonPathQueriesProvider.getJsonPathQueries().size()>0) {

								for(String query : JsonPathQueriesProvider.getJsonPathQueries()) {
									writeToComparisonFile("\nComparison result for JsonPath : "+query);

									leftJson =JsonPath.using(conf).parse(leftJsonFull).read(query).toString();
									rightJson=JsonPath.using(conf).parse(rightJsonFull).read(query).toString();

									if(rightJson.isEmpty() || leftJson.isEmpty()) {
										writeToComparisonFile("Couldn't find any result for the jsonQuery !!");
									}else {
											try {
												ObjectMapper mapper = new ObjectMapper();
												TypeReference<HashMap<String, Object>> type = new TypeReference<HashMap<String, Object>>() {};

												Map<String, Object> leftMap = mapper.readValue(leftJson, type);
												Map<String, Object> rightMap = mapper.readValue(rightJson, type);

												Map<String, Object> leftFlatMap = FlatMapUtil.flatten(leftMap);
												Map<String, Object> rightFlatMap = FlatMapUtil.flatten(rightMap);

												MapDifference<String, Object> difference = Maps.difference(leftFlatMap, rightFlatMap);

												if(difference.entriesOnlyOnLeft().size()>0) {
												writeToComparisonFile("\nEntries only in Original Response\n--------------------------");
												difference.entriesOnlyOnLeft().forEach((key, value) -> writeToComparisonFile("Original-" +key + ": " + value));
												}
												
												if(difference.entriesOnlyOnRight().size()>0) {
												writeToComparisonFile("\n\nEntries only in Replayed Response\n--------------------------");
												difference.entriesOnlyOnRight().forEach((key, value) -> writeToComparisonFile("Replayed-" + key + ": " + value));
												}
												
												if(difference.entriesDiffering().size()>0) {
												writeToComparisonFile("\n\nEntries differing\n--------------------------");
												difference.entriesDiffering().forEach((key, value) -> writeToComparisonFile("Diff-" + key + ": " + value));
												}
												
												if(!differs) {
													if(difference.entriesDiffering().size()>0)
														differs=true;
													else if(difference.entriesOnlyOnLeft().size()>0)
														differs=true;
													else if(difference.entriesOnlyOnRight().size()>0) {
														if(!ConfigReader.shouldIgnoreNewKeys())
															differs=true;
													}
												}
											}catch(com.fasterxml.jackson.databind.exc.MismatchedInputException e) {
												ObjectMapper mapper2 = new ObjectMapper();
												TypeReference<Object> type2 = new TypeReference<Object>() {};

												Object left = mapper2.readValue(leftJson, type2);
												Object right = mapper2.readValue(rightJson, type2);

												writeToComparisonFile("Original-  "+left);
												writeToComparisonFile("Replayed- "+right);

												if(!differs) {
													if(!left.equals(right)) {
														differs = true;
													}
												}
											}
									}

								}

							}
							else {
								leftJson = leftJsonFull;
								rightJson = rightJsonFull;

								if(leftJson.isEmpty() || rightJson.isEmpty()) {
									writeToComparisonFile("Json response is empty ! Nothing to compare in original and replayed response");
								}else {
									try {
										ObjectMapper mapper = new ObjectMapper();
										TypeReference<HashMap<String, Object>> type = new TypeReference<HashMap<String, Object>>() {};

										Map<String, Object> leftMap = mapper.readValue(leftJson, type);
										Map<String, Object> rightMap = mapper.readValue(rightJson, type);

										Map<String, Object> leftFlatMap = FlatMapUtil.flatten(leftMap);
										Map<String, Object> rightFlatMap = FlatMapUtil.flatten(rightMap);

										MapDifference<String, Object> difference = Maps.difference(leftFlatMap, rightFlatMap);

										if(difference.entriesOnlyOnLeft().size()>0) {
										writeToComparisonFile("Entries only in Original Response\n--------------------------");
										difference.entriesOnlyOnLeft().forEach((key, value) -> {
												writeToComparisonFile("Original-" +key + ": " + value);
											});
										}
										
										
										if(difference.entriesOnlyOnRight().size()>0) {
										writeToComparisonFile("\n\nEntries only in Replayed Response\n--------------------------");
										difference.entriesOnlyOnRight().forEach((key, value) -> writeToComparisonFile("Replayed-" + key + ": " + value));
										}
										
										if(difference.entriesDiffering().size()>0) {
										writeToComparisonFile("\n\nEntries differing\n--------------------------");
										difference.entriesDiffering().forEach((key, value) -> {
													writeToComparisonFile("Diff-" + key + ": " + value);
										});
										}

										if(!differs) {
											if(difference.entriesDiffering().size()>0) {
													differs = true;
											}							
											else if(difference.entriesOnlyOnLeft().size()>0) {
													differs=true;
											}
											else if(difference.entriesOnlyOnRight().size()>0) {
												if(!ConfigReader.shouldIgnoreNewKeys())
													differs=true;
											}
										}
										
									}catch(com.fasterxml.jackson.databind.exc.MismatchedInputException e) {
										ObjectMapper mapper2 = new ObjectMapper();
										TypeReference<Object> type2 = new TypeReference<Object>() {};

										Object left = mapper2.readValue(leftJson, type2);
										Object right = mapper2.readValue(rightJson, type2);

										writeToComparisonFile("Original-  "+left);
										writeToComparisonFile("Replayed- "+right);

										if(!differs) {
											if(!left.equals(right)) {
												differs = true;
											}
										}
									}
								}

							}

							if(!differs) {
								passCount++;
								writeToComparisonFile("      Original and replayed responses are same!");
								writeToComparisonFile("\nURL is -- "+url);
								writeToLogFile("  responses are same");
							}
							writeToComparisonFile("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					}
					catch(Exception e){
						exceptionCount++;
						isExceptionOccured=true;
						writeToComparisonFile(e.getMessage());
						for(StackTraceElement t : e.getStackTrace()) {
							writeToComparisonFile("\n"+t.getFileName()+", "+t.getClassName()+", "+t.getMethodName()+" ,("+t.getLineNumber()+")");
						}
						writeToComparisonFile("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
						writeToFiles(requestID, url, response_server1.asString(), response_server2.asString());
						writeToLogFile("   exception occured - "+e.getMessage());
					}


					if (differs)
					{	
						if(!isExceptionOccured) {
							failCount++;
							writeToLogFile("    had differering response bodies.");
							writeToFiles(requestID, url, response_server1.asString(), response_server2.asString());
						}
					}

				}
			}catch(Exception e) {
				exceptionCount++;
				writeToLogFile("   exception occured == "+e.getMessage());
			}

			urlsTraversed.add(url);
		}

	}



	private void writeToFiles(String requestID, String request, String response_server1, String response_server2) throws IOException {
		File requestFile = new File(this.dirName+"/"+requestID+"_"+"req.txt");
		FileWriter fr1 = new FileWriter(requestFile);
		fr1.write(request);
		fr1.flush();

		File resServer1File = new File(this.dirName+"/"+requestID+"_"+"server1.txt");
		FileWriter fr2 = new FileWriter(resServer1File);
		fr2.write(response_server1);
		fr2.flush();

		File resServer2File = new File(this.dirName+"/"+requestID+"_"+"server2.txt");
		FileWriter fr3 = new FileWriter(resServer2File);
		fr3.write(response_server2);
		fr3.flush();

		try {
			fr1.close();
			fr2.close();
			fr3.close();
		}catch(Exception e) {
		}
	}

	private void writeToLogFile(String text) throws IOException {
		logWriter.write(text+"\n");
		logWriter.flush();
		if(logFile.length()/1000 >= logFileMaxSizeAllowedInKB) {
			logFile = getNewLogFile();
			logWriter = new FileWriter(logFile);
		}
	}

	private void writeToComparisonFile(String text) {
		try {
			crWriter.write(text+"\n");
			crWriter.flush();
			if(comparisonResultFile.length()/1000 >= comparisonResultMaxSizeAllowedInKB) {
				comparisonResultFile = getNewComparisonFile();
				crWriter = new FileWriter(comparisonResultFile);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	private File getNewLogFile() throws IOException {
		int logFileIndex = getLargestFileIndexInDirectory(dirName, "log_");
		File file = new File(dirName+"/"+"log_"+(logFileIndex+1)+".txt");
		if(!file.exists())
			file.createNewFile();
		return file;
	}

	private File getNewComparisonFile() throws IOException {
		int logFileIndex = getLargestFileIndexInDirectory(dirName, "ComparisonResult_");
		File file = new File(dirName+"/"+"ComparisonResult_"+(logFileIndex+1)+".txt");
		if(!file.exists())
			file.createNewFile();
		return file;
	}

	private static int getLargestFileIndexInDirectory(String dirName, String filter) {
		File directory = new File(dirName);
		File[] files = directory.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if(name.contains(filter)) return true;
				else return false;
			}
		});

		int indexToReturn = 0;
		for(File currentFile : files) {
			String fileName = currentFile.getName();
			int currentFileIndex = Integer.parseInt(fileName.substring(
					fileName.lastIndexOf("_")+1,
					fileName.lastIndexOf(".")));
			if(currentFileIndex > indexToReturn) {
				indexToReturn = currentFileIndex;
			}
		}

		return indexToReturn;
	} 

	private String requestIDGenerator() {
		int count = 40;
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
	
	public void writeStatusToStatusFile() {
		try {
			File file = new File(this.dirName+"/"+"status.txt");
			if(!file.exists())
				file.createNewFile();
			FileWriter fw = new FileWriter(file);
			fw.write("==========================================================\n\n");
			fw.write("Total Hits :  "+totalCount+"\n");
			fw.write("Success    :  "+passCount+"\n");
			fw.write("Failed     :  "+failCount+"\n");
			fw.write("Exceptions :  "+exceptionCount+"\n");
			fw.write("\n==========================================================");
			fw.flush();
			fw.close();
		}catch(Exception e) {
			System.out.println("Error occured while writing to status file!");
			e.printStackTrace();
		}
	}

}
